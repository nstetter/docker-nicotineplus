# A simple docker image for nicotine++

This is a docker image that runs nicotine++ on a headless VNC server. The main
effort is done in the [baseimage](https://github.com/jlesage/docker-baseimage-gui).
