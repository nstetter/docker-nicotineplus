# Pull base image.
FROM jlesage/baseimage-gui:ubuntu-24.04-v4.7.1

# Install nicotine-plus.
RUN apt-get update && apt-get install -y \
        software-properties-common \
        gnupg2
RUN add-apt-repository ppa:nicotine-team/stable && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6CEB6050A30E5769 && \
    apt-get update && \
    apt-get install -y \
        # find package version in PPA: https://launchpad.net/~nicotine-team/+archive/ubuntu/stable
        nicotine=3.3.10-202503101356~ubuntu24.04.1&& \
    rm -rf /var/lib/apt/lists/*

# Copy the start script.
COPY rootfs/startapp.sh /startapp.sh

# Set the name of the application.
ENV APP_NAME="Nicotine++"
